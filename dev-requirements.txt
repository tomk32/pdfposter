pip
setuptools
wheel

PyPDF2

# for testing
reportlab
tox

# for building the documentation
sphinx
